package main

import (
	"flag"
	"git.snt.utwente.nl/dingen/moonshine/rpl/controlmessage"
	"golang.org/x/net/icmp"
	"golang.org/x/net/ipv6"
	"log"
	"net"
)

func main() {
	// Command line arguments
	var address string
	flag.StringVar(&address, "a", "::", "Address to listen on")
	flag.Parse()

	// Resolve address to listen on
	netAddr, _ := net.ResolveIPAddr("ip6", address)

	// Start listening
	ln, err := net.ListenIP("ip6:58", netAddr)
	if err != nil {
		log.Fatalf("Error: %s", err.Error())
	}

	// Close the connection on exit
	defer ln.Close()

	// Start
	log.Printf("Listening on %s", netAddr)

	// Start reading
	buf := make([]byte, 2048)
	for {
		// Read the socket into the buffer
		numRead, addr, err := ln.ReadFrom(buf)
		if err != nil {
			log.Printf("Error: %s", err)
			continue
		}

		// Parse the ICMP message
		i, _ := icmp.ParseMessage(58, buf[:numRead])

		// Show the message
		log.Printf("%s - %s", addr, i.Type)

		// Show more information for RPL messages
		if i.Type == ipv6.ICMPTypeRPLControl {
			r, err := controlmessage.ParseICMPMessage(i)
			if err != nil {
				log.Printf("Error: %s", err)
				continue
			}

			log.Printf("%+v", r)
		}
	}
}
