package controlmessage

type DAGMetricContainer struct {
	Data []byte
}

const (
	DAGMetricContainerType = OptionTypeDAGMetricContainer
)

func (o *DAGMetricContainer) Len() int {
	return OptionHeaderLength + len(o.Data)
}

func (o *DAGMetricContainer) Type() OptionType {
	return DAGMetricContainerType
}

func (o *DAGMetricContainer) Marshal() (b []byte, err error) {
	// Assemble packet
	b = []byte{
		byte(DAGMetricContainerType),
		byte(len(o.Data)),
	}

	// Append data
	b = append(b, o.Data...)

	return
}

func parseDAGMetricContainer(b []byte) (o Option, err error) {
	// Given bytes are the metric data
	o = &DAGMetricContainer{
		Data: b[2:],
	}

	return
}
