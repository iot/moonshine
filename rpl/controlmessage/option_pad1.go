package controlmessage

type Pad1 struct{}

const (
	Pad1Type         = OptionTypePad1
	Pad1PacketLength = 1
)

func (o *Pad1) Len() int {
	return Pad1PacketLength
}

func (o *Pad1) Type() OptionType {
	return Pad1Type
}

func (o *Pad1) Marshal() (b []byte, err error) {

	// Pad1 only has a type
	b = []byte{
		byte(Pad1Type),
	}

	return
}

func parsePad1(b []byte) (Option, error) {
	// Check length
	if len(b) != 1 {
		return nil, &SizeError{len(b), 1}
	}

	return &Pad1{}, nil
}
