package controlmessage

import (
	"encoding/binary"
	"errors"
	"golang.org/x/net/icmp"
	"golang.org/x/net/ipv6"
	"fmt"
)

type Message struct {
	Type     ipv6.ICMPType
	Code     Type
	Checksum int
	Base     Base
	Options  Options
}

type Base interface {
	Len() int
	Type() Type
	Marshal() ([]byte, error)
}

type Type byte

const (
	TypeDIS          Type = 0x00
	TypeDIO          Type = 0x01
	TypeDAO          Type = 0x02
	TypeDAOAck       Type = 0x03
	TypeSecureDIS    Type = 0x80
	TypeSecureDIO    Type = 0x81
	TypeSecureDAO    Type = 0x82
	TypeSecureDAOAck Type = 0x83
	TypeCC           Type = 0x8A
)

var types = map[Type]string{
	TypeDIS:    "DIS",
	TypeDIO:    "DIO",
	TypeDAO:    "DAO",
	TypeDAOAck: "DAO-ACK",
	TypeCC:     "CC",
}

var parseFns = map[Type]func([]byte) (Base, error){
	TypeDIS:    ParseDIS,
	TypeDIO:    ParseDIO,
	TypeDAO:    ParseDAO,
	TypeDAOAck: ParseDAOAck,
	TypeCC:     ParseCC,
}

func (typ Type) String() string {
	s, ok := types[typ]
	if !ok {
		return "<nil>"
	}
	return s
}

func ParseMessage(b []byte) (m *Message, err error) {
	// Fill ICMP fields from data
	m = &Message{
		Type:     ipv6.ICMPType(b[0]),
		Code:     Type(b[1]),
		Checksum: int(binary.BigEndian.Uint16(b[2:4])),
	}

	// Check if message is a RPL message
	if m.Type != ipv6.ICMPTypeRPLControl {
		return m, &NotRPLError{}
	}

	// Strip headers
	b = b[4:]

	// Parse base
	m.Base, err = ParseBase(b, m.Code)
	if err != nil {
		return
	}

	// Parse options
	m.Options, err = ParseOptions(b[m.Base.Len():])

	return
}

func ParseICMPMessage(i *icmp.Message) (m *Message, err error) {
	// Create message with similar parts
	m = &Message{
		Type:     i.Type.(ipv6.ICMPType),
		Code:     Type(i.Code),
		Checksum: i.Checksum,
	}

	// Get body from Default Message Body
	body := i.Body.(*icmp.DefaultMessageBody).Data

	// Parse base
	m.Base, err = ParseBase(body, Type(i.Code))
	if err != nil {
		return
	}

	// Parse options
	m.Options, err = ParseOptions(body[m.Base.Len():])

	return
}

func ParseBase(b []byte, c Type) (d Base, err error) {
	if fn, ok := parseFns[c]; ok {
		d, err = fn(b)
	} else {
		err = errors.New("Invalid message")
	}
	return
}

func (m *Message) String() string {
	s := fmt.Sprintf("Type: %s, Code: %s, Options:", m.Type, m.Code)

	for i,o := range m.Options {
		s += fmt.Sprintf(" %s", o.Type())
		if i < len(m.Options) - 1 {
			s+=","
		}
	}

	return s
}

func (m *Message) Marshal(psh []byte) (b []byte, err error) {
	var base, opts []byte
	b = make([]byte, 4)

	// Set type if unset
	if m.Type == 0 {
		m.Type = ipv6.ICMPTypeRPLControl
	}

	// Set code if unset (or DIS)
	if m.Code == 0 {
		m.Code = m.Base.Type()
	}

	b[0] = byte(m.Type)
	b[1] = byte(m.Code)

	// TODO: Calculate the checksum from `psh`
	binary.BigEndian.PutUint16(b[2:4], uint16(m.Checksum))

	// Append base
	base, err = m.Base.Marshal()
	b = append(b, base...)

	// Append options
	opts, err = m.Options.Marshal()
	b = append(b, opts...)

	return
}
