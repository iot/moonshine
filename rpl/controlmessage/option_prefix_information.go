package controlmessage

import (
	"encoding/binary"
	"net"
)

type PrefixInformation struct {
	PrefixLength      uint8
	L                 bool
	A                 bool
	R                 bool
	ValidLifetime     uint32
	PreferredLifetime uint32
	Prefix            net.IP
}

const (
	PrefixInformationType         = OptionTypePrefixInformation
	PrefixInformationPrefixLength = 16
	PrefixInformationLength       = 30
	PrefixInformationPacketLength = OptionHeaderLength + PrefixInformationLength
)

func (o *PrefixInformation) Len() int {
	return PrefixInformationPacketLength
}

func (o *PrefixInformation) Type() OptionType {
	return PrefixInformationType
}

func (o *PrefixInformation) Marshal() (b []byte, err error) {
	// Check prefix size
	if len(o.Prefix) > PrefixInformationPrefixLength {
		return nil, &SizeError{len(o.Prefix), PrefixInformationPrefixLength}
	}

	// Create data
	b = make([]byte, o.Len())
	b[0] = byte(PrefixInformationType)
	b[1] = PrefixInformationLength
	b[2] = o.PrefixLength
	b[3] = BoolToByte(o.L)<<7 | BoolToByte(o.A)<<6 | BoolToByte(o.R)<<5

	// Put unsigned integers in their place
	binary.BigEndian.PutUint32(b[4:8], o.ValidLifetime)
	binary.BigEndian.PutUint32(b[8:12], o.PreferredLifetime)

	// Copy prefix
	copy(b[16:32], o.Prefix)

	return
}

func parsePrefixInformation(b []byte) (o Option, err error) {
	// Check data size
	if len(b) != PrefixInformationPacketLength {
		return nil, &SizeError{len(b), PrefixInformationPacketLength}
	}

	// Create object
	o = &PrefixInformation{
		PrefixLength:      b[2],
		L:                 ByteToBool(b[3] >> 7),
		A:                 ByteToBool(b[3] >> 6),
		R:                 ByteToBool(b[3] >> 5),
		ValidLifetime:     binary.BigEndian.Uint32(b[4:8]),
		PreferredLifetime: binary.BigEndian.Uint32(b[8:12]),
		Prefix:            b[16:32],
	}

	return
}
