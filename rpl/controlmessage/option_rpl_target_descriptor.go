package controlmessage

import "encoding/binary"

type RPLTargetDescriptor struct {
	Descriptor uint32
}

const (
	RPLTargetDescriptorType             = OptionTypeRPLTargetDescriptor
	RPLTargetDescriptorLength       int = 4
	RPLTargetDescriptorPacketLength     = OptionHeaderLength + RPLTargetDescriptorLength
)

func (o *RPLTargetDescriptor) Len() int {
	return RPLTargetDescriptorPacketLength
}

func (o *RPLTargetDescriptor) Type() OptionType {
	return RPLTargetDescriptorType
}

func (o *RPLTargetDescriptor) Marshal() (b []byte, err error) {

	// Create data fields
	b = make([]byte, RPLTargetDescriptorPacketLength)
	b[0] = byte(RPLTargetDescriptorType)
	b[1] = byte(RPLTargetDescriptorLength)
	binary.BigEndian.PutUint32(b[2:], o.Descriptor)

	return
}

func parseRPLTargetDescriptor(b []byte) (o Option, err error) {
	// Check data size
	if len(b) != RPLTargetDescriptorPacketLength {
		return nil, &SizeError{len(b), RPLTargetDescriptorPacketLength}
	}

	// Create object
	o = &RPLTargetDescriptor{
		Descriptor: binary.BigEndian.Uint32(b[2:]),
	}

	return
}
