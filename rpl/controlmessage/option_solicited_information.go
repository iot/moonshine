package controlmessage

import "net"

type SolicitedInformation struct {
	V             bool
	I             bool
	D             bool
	VersionNumber uint8
	RPLInstanceID uint8
	DODAGID       net.IP
}

const (
	SolicitedInformationType             = OptionTypeSolicitedInformation
	SolicitedInformationLength       int = 19
	SolicitedInformationPacketLength     = OptionHeaderLength + SolicitedInformationLength
)

func (o *SolicitedInformation) Len() int {
	return SolicitedInformationPacketLength
}

func (o *SolicitedInformation) Type() OptionType {
	return SolicitedInformationType
}

func (o *SolicitedInformation) Marshal() (b []byte, err error) {
	// Check size of DODAGID
	if len(o.DODAGID) != 16 {
		return nil, &InvalidDODAGIDError{o.DODAGID}
	}

	// Assemble bytes
	b = make([]byte, SolicitedInformationPacketLength)
	b[0] = byte(SolicitedInformationType)
	b[1] = byte(SolicitedInformationLength)
	b[2] = o.RPLInstanceID
	b[3] = BoolToByte(o.V)<<7 | BoolToByte(o.I)<<6 | BoolToByte(o.D)<<5
	b[18] = o.VersionNumber

	// Add in DODAGID
	copy(b[4:18], o.DODAGID)

	// Append Version Number
	b = append(b, o.VersionNumber)

	return
}

func parseSolicitedInformation(b []byte) (o Option, err error) {
	// Check data size
	if len(b) != SolicitedInformationLength {
		return nil, &SizeError{len(b), SolicitedInformationLength}
	}

	// Create object from data
	o = &SolicitedInformation{
		V:             ByteToBool(b[3] >> 7),
		I:             ByteToBool(b[3] >> 6),
		D:             ByteToBool(b[3] >> 5),
		RPLInstanceID: b[2],
		DODAGID:       b[4:20],
		VersionNumber: b[21],
	}

	return
}
