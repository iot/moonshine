package controlmessage

type PadN struct {
	N int
}

const (
	PadNType      = OptionTypePadN
	PadNLengthMin = OptionHeaderLength
	PadNLengthMax = 7
)

func (o *PadN) Type() OptionType {
	return PadNType
}

func (o *PadN) Len() int {
	return int(o.N)
}

func (o *PadN) Marshal() (b []byte, err error) {
	// Check length
	if o.N < PadNLengthMin || o.N > PadNLengthMax {
		err = &SizeRangeError{o.N, PadNLengthMin, PadNLengthMax}
		return
	}

	// Set length
	length := uint8(o.N - PadNLengthMin)

	// Assemble packet
	b = []byte{
		byte(PadNType),
		length,
	}

	// Append data
	d := make([]byte, length)
	b = append(b, d...)

	return
}

func parsePadN(b []byte) (Option, error) {
	// Check length
	if len(b) > PadNLengthMax {
		return nil, &SizeRangeError{len(b), PadNLengthMin, PadNLengthMax}
	}

	// Create object
	return &PadN{len(b)}, nil
}
