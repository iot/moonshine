package controlmessage

import "net"

type DAOAck struct {
	RPLInstanceID uint8
	D             bool
	Sequence      uint8
	Status        uint8
	DODAGID       net.IP
}

const (
	DAOAckLength int = 4
)

func (d *DAOAck) Len() int {
	return DAOAckLength + len(d.DODAGID)
}

func (d *DAOAck) Type() Type {
	return TypeDAO
}

// See RFC6550 section 6.5.1.
func (d *DAOAck) Marshal() (b []byte, err error) {
	b = make([]byte, DAOAckLength+len(d.DODAGID))

	// Set values
	b[0] = d.RPLInstanceID
	b[1] = BoolToByte(d.D) << 7
	b[2] = d.Sequence
	b[3] = d.Status

	// Copy DODAGID
	if d.D {
		copy(b[DAOLength:], d.DODAGID)
	}

	return
}

// See RFC6550 section 6.5.1.
func ParseDAOAck(b []byte) (Base, error) {
	var err error
	d := new(DAOAck)

	// Check data size
	if len(b) < DAOAckLength {
		return d, &SizeError{len(b), DAOAckLength}
	}

	// Set fields from data
	d.RPLInstanceID = b[0]
	d.D = ByteToBool(b[1] >> 7)
	d.Sequence = b[2]
	d.Status = b[3]

	// Store DODAGID if required
	if d.D {
		d.DODAGID = b[DAOAckLength : DAOAckLength+16]
	}

	return d, err
}
