package controlmessage

import (
	"encoding/binary"
	"errors"
)

type RoutingInformation struct {
	Prf           int8
	RouteLifeTime uint32
	Prefix        []byte
}

const (
	RoutingInformationType                   = OptionTypeRoutingInformation
	RoutingInformationHeaderLength       int = 6
	RoutingInformationPacketHeaderLength     = OptionHeaderLength + RoutingInformationHeaderLength
)

func (o *RoutingInformation) Len() int {
	return RoutingInformationPacketHeaderLength + len(o.Prefix)
}

func (o *RoutingInformation) Type() OptionType {
	return RoutingInformationType
}

func (o *RoutingInformation) Marshal() (b []byte, err error) {
	// Check Prf values
	if o.Prf < -1 || o.Prf > 1 {
		return nil, &SizeRangeError{int(o.Prf), -1, 1}
	}

	// Create the header
	b = make([]byte, RoutingInformationPacketHeaderLength)
	b[0] = byte(RoutingInformationType)
	b[1] = byte(RoutingInformationHeaderLength + len(o.Prefix))
	b[2] = byte(len(o.Prefix))
	b[3] = byte(o.Prf<<3) & 0x18

	// Add the route lifetime
	binary.BigEndian.PutUint32(b[4:], o.RouteLifeTime)

	// Append the prefix
	b = append(b, o.Prefix...)

	return
}

func parseRoutingInformation(b []byte) (Option, error) {
	// Get prefix length from third byte
	pLen := int(b[2])

	// Check size
	if len(b) != pLen+RoutingInformationPacketHeaderLength {
		return nil, &SizeError{len(b), pLen + RoutingInformationPacketHeaderLength}
	}

	// Create Routing information object
	o := &RoutingInformation{
		Prf:           int8(b[3]&0x18<<3) >> 6,
		RouteLifeTime: binary.BigEndian.Uint32(b[4:8]),
		Prefix:        b[8 : 8+pLen],
	}

	// Prf cannot be -2
	if o.Prf == -2 {
		return nil, errors.New("Invalid Routing Information Option")
	}

	return o, nil
}
