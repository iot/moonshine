package controlmessage

import (
	"bytes"
	"net"
	"testing"
)

func TestThis(t *testing.T) {
	m := &Message{
		Base: &DIO{
			Grounded: true,
			DODAGID:  net.ParseIP("2001:db8::ff"),
		},
		Options: []Option{
			&PadN{6},
		},
	}

	// Test marshal of a given message
	b, _ := m.Marshal(nil)
	bExp := []byte{
		0x9b, 0x1, 0x0, 0x0, // ICMP Header
		0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, // Base of message
		0x20, 0x01, 0x0d, 0xb8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0xff, // DODAGID
		0x1, 0x4, 0x0, 0x0, 0x0, 0x0, // Options
	}

	if !bytes.Equal(b, bExp) {
		t.Errorf(
			"Generated bytestream does not match given for DIO:\n"+
				"Got:       %v\n"+
				"Should be: %v\n",
			b, bExp,
		)
	}

	// Test message
	msg := []byte{0x9b, 0x00, 0x5f, 0xa5, 0x00, 0x00, 0x01, 0x02, 0x00, 0x00}

	// Parse message back and forth
	x, _ := ParseMessage(msg)
	b, _ = x.Marshal(nil)

	// Compare output
	if !bytes.Equal(msg, b) {
		t.Errorf(
			"Generated bytestream does not match given for DIS:\n"+
				"Got:       %v\n"+
				"Should be: %v\n",
			b, msg,
		)
	}
}
