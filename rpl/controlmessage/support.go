package controlmessage

// BoolToByte converts a boolean `t` to the LSB of a byte.
func BoolToByte(t bool) byte {
	if t {
		return 1
	}
	return 0
}

// ByteToBool converts the LSB of the given byte `b` to a boolean.
func ByteToBool(b byte) bool {
	return (b & 1) == 1
}
