package controlmessage

import (
	"golang.org/x/net/icmp"
)

type Secure struct {
	Type     icmp.Type
	Code     Type
	Checksum int
	Security SecurityFields
	Base     []byte
	Options  []byte
}

type SecurityFields struct {
	CounterIsTime     bool
	Algorithm         SecurityAlgorithm
	KeyIdentifierMode SecurityKIM
	Level             SecurityLevel
	Flags             byte
	Counter           int
	KeyIdentifier     SecurityKeyIdentifier
}

type SecurityAlgorithm int

const (
	CCMAES128RSHASHA256 SecurityAlgorithm = 0
)

type SecurityKIM int

const (
	GroupKeyIndex  SecurityKIM = 0
	PairKeySource  SecurityKIM = 1
	GroupKeySource SecurityKIM = 2
	SignatureKey   SecurityKIM = 3
)

type SecurityLevel int

const (
	MAC32       SecurityLevel = 0
	ENCMAC32    SecurityLevel = 1
	MAC64       SecurityLevel = 2
	ENCMAC64    SecurityLevel = 3
	MAC32Len    int           = 4
	ENCMAC32Len int           = 4
	MAC64Len    int           = 8
	ENCMAC64Len int           = 8

	Sign3072       SecurityLevel = 0
	ENCSign3072    SecurityLevel = 1
	Sign2048       SecurityLevel = 2
	ENCSign2048    SecurityLevel = 3
	Sign3072Len    int           = 384
	ENCSign3072Len int           = 384
	Sign2048Len    int           = 256
	ENCSign2048Len int           = 256
)

type SecurityKeyIdentifier struct {
	Source *[8]byte
	Index  byte
}
