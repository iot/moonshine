package controlmessage

import (
	"fmt"
	"net"
)

type SizeError struct {
	received int
	required int
}

func (e *SizeError) Error() string {
	return fmt.Sprintf("Data has incorrect size: %d instead of %d", e.received, e.required)
}

type SizeRangeError struct {
	received int
	min      int
	max      int
}

func (e *SizeRangeError) Error() string {
	return fmt.Sprintf("Data has incorrect size: %d instead of between %d and %d", e.received, e.min, e.max)
}

type NotRPLError struct{}

func (e *NotRPLError) Error() string {
	return "ICMP message is not a RPL message"
}

type InvalidDODAGIDError struct {
	DODAGID net.IP
}

func (e *InvalidDODAGIDError) Error() string {
	return fmt.Sprintf("DODAGID is not valid: %s", e.DODAGID.String())
}
