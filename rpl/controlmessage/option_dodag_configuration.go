package controlmessage

import "encoding/binary"

type DODAGConfiguration struct {
	AuthenticationEnabled bool
	PathControlSize       uint8
	DIOIntervalDoublings  uint8
	DIOIntervalMin        uint8
	DIORedundancyConstant uint8
	MaxRankIncrease       uint16
	MinHopRankIncrease    uint16
	ObjectiveCodePoint    uint16
	DefaultLifetime       uint8
	LifetimeUnit          uint16
}

const (
	DODAGConfigurationType         = OptionTypeDODAGConfiguration
	DODAGConfigurationLength       = 14
	DODAGConfigurationPacketLength = OptionHeaderLength + DODAGConfigurationLength
)

func (o *DODAGConfiguration) Len() int {
	return DODAGConfigurationPacketLength
}

func (o *DODAGConfiguration) Type() OptionType {
	return DODAGConfigurationType
}

func (o *DODAGConfiguration) Marshal() (b []byte, err error) {
	// Create byte data
	b = make([]byte, o.Len())
	b[0] = byte(DODAGConfigurationType)
	b[1] = DODAGConfigurationLength
	b[2] = BoolToByte(o.AuthenticationEnabled)<<3 | o.PathControlSize&0x07
	b[3] = o.DIOIntervalDoublings
	b[4] = o.DIOIntervalMin
	b[5] = o.DIORedundancyConstant
	b[13] = o.DefaultLifetime

	// Convert 16 bit values
	binary.BigEndian.PutUint16(b[6:8], o.MaxRankIncrease)
	binary.BigEndian.PutUint16(b[8:10], o.MinHopRankIncrease)
	binary.BigEndian.PutUint16(b[10:12], o.ObjectiveCodePoint)
	binary.BigEndian.PutUint16(b[14:16], o.LifetimeUnit)

	return
}

func parseDODAGConfiguration(b []byte) (o Option, err error) {
	// Check size of bytes
	if len(b) != DODAGConfigurationPacketLength {
		return nil, &SizeError{len(b), DODAGConfigurationPacketLength}
	}

	// Create object
	o = &DODAGConfiguration{
		AuthenticationEnabled: ByteToBool(b[2] >> 3),
		PathControlSize:       b[2] & 0x07,
		DIOIntervalDoublings:  b[3],
		DIOIntervalMin:        b[4],
		DIORedundancyConstant: b[5],
		MaxRankIncrease:       binary.BigEndian.Uint16(b[6:8]),
		MinHopRankIncrease:    binary.BigEndian.Uint16(b[8:10]),
		ObjectiveCodePoint:    binary.BigEndian.Uint16(b[10:12]),
		DefaultLifetime:       b[13],
		LifetimeUnit:          binary.BigEndian.Uint16(b[14:16]),
	}

	return
}
