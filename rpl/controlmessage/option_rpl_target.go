package controlmessage

type RPLTarget struct {
	Prefix []byte
}

const (
	RPLTargetType               = OptionTypeRPLTarget
	RPLTargetHeaderLength       = 1
	RPLTargetPacketHeaderLength = OptionHeaderLength + RPLTargetHeaderLength
)

func (o *RPLTarget) Len() int {
	return RPLTargetPacketHeaderLength + len(o.Prefix)
}

func (o *RPLTarget) Type() OptionType {
	return RPLTargetType
}

func (o *RPLTarget) Marshal() (b []byte, err error) {
	// Assemble header
	b = []byte{
		byte(RPLTargetType),
		byte(RPLTargetHeaderLength + len(o.Prefix)),
		0,
		byte(len(o.Prefix)),
	}

	// Append prefix
	b = append(b, o.Prefix...)

	return
}

func parseRPLTarget(b []byte) (o Option, err error) {
	// Check length of prefix
	pLen := int(b[2])
	if len(b) != pLen+RPLTargetPacketHeaderLength {
		return nil, &SizeError{len(b), pLen + RPLTargetPacketHeaderLength}
	}

	// Create object
	o = &RPLTarget{
		Prefix: b[3:],
	}
	return
}
