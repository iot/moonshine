package controlmessage

import (
	"encoding/binary"
	"net"
)

type CC struct {
	RPLInstanceID      uint8
	R                  bool
	Nonce              uint16
	DODAGID            net.IP
	DestinationCounter uint32
}

type CCOption byte

const (
	CCLength int = 24
)

func (d *CC) Len() int {
	return CCLength
}

func (d *CC) Type() Type {
	return TypeCC
}

// See RFC6550 section 6.6.1.
func (d *CC) Marshal() (b []byte, err error) {
	b = make([]byte, CCLength)

	// Set values
	b[0] = d.RPLInstanceID
	b[1] = BoolToByte(d.R) << 7

	// Set nonce
	binary.BigEndian.PutUint16(b[2:4], d.Nonce)

	// Copy DODAGID
	copy(b[4:20], d.DODAGID)

	// Set destination counter
	binary.BigEndian.PutUint32(b[20:24], d.DestinationCounter)

	return
}

// See RFC6550 section 6.4.1.
func ParseCC(b []byte) (Base, error) {
	var err error
	d := new(CC)

	// Check bytes
	if len(b) < CCLength {
		return d, &SizeError{len(b), CCLength}
	}

	// Get values from data
	d.RPLInstanceID = b[0]
	d.R = ByteToBool(b[1] >> 7)
	d.Nonce = binary.BigEndian.Uint16(b[2:4])
	d.DODAGID = b[4:20]
	d.DestinationCounter = binary.BigEndian.Uint32(b[20:24])

	return d, err
}
