package controlmessage

type OptionType byte

const (
	OptionHeaderLength int = 2

	OptionTypePad1                 OptionType = 0x00
	OptionTypePadN                 OptionType = 0x01
	OptionTypeDAGMetricContainer   OptionType = 0x02
	OptionTypeRoutingInformation   OptionType = 0x03
	OptionTypeDODAGConfiguration   OptionType = 0x04
	OptionTypeRPLTarget            OptionType = 0x05
	OptionTypeTransitInformation   OptionType = 0x06
	OptionTypeSolicitedInformation OptionType = 0x07
	OptionTypePrefixInformation    OptionType = 0x08
	OptionTypeRPLTargetDescriptor  OptionType = 0x09
)

var options = map[OptionType]string{
	OptionTypePad1:                 "Pad 1",
	OptionTypePadN:                 "Pad N",
	OptionTypeDAGMetricContainer:   "DAG Metric Container",
	OptionTypeRoutingInformation:   "Routing Information",
	OptionTypeDODAGConfiguration:   "DODAG Configuration",
	OptionTypeRPLTarget:            "RPL Target",
	OptionTypeTransitInformation:   "Transit Information",
	OptionTypeSolicitedInformation: "Solicited Information",
	OptionTypePrefixInformation:    "Prefix Information",
	OptionTypeRPLTargetDescriptor:  "RPL Target Descriptor",
}

var parseOptionFns = map[OptionType]func([]byte) (Option, error){
	OptionTypePad1:                 parsePad1,
	OptionTypePadN:                 parsePadN,
	OptionTypeDAGMetricContainer:   parseDAGMetricContainer,
	OptionTypeRoutingInformation:   parseRoutingInformation,
	OptionTypeDODAGConfiguration:   parseDODAGConfiguration,
	OptionTypeRPLTarget:            parseRPLTarget,
	OptionTypeTransitInformation:   parseTransitInformation,
	OptionTypeSolicitedInformation: parseSolicitedInformation,
	OptionTypePrefixInformation:    parsePrefixInformation,
	OptionTypeRPLTargetDescriptor:  parseRPLTargetDescriptor,
}

func (typ OptionType) String() string {
	s, ok := options[typ]
	if !ok {
		return "<nil>"
	}
	return s
}

type Option interface {
	Len() int
	Type() OptionType
	Marshal() ([]byte, error)
}

type Options []Option

func (opts Options) Len() int {
	l := 0

	for _, o := range opts {
		l += o.Len()
	}

	return l
}

func (opts Options) Marshal() (b []byte, err error) {
	var d []byte
	b = make([]byte, 0)

	// Marshal all options
	for _, o := range opts {
		d, err = o.Marshal()

		b = append(b, d...)
	}

	return
}

func ParseOption(b []byte) (Option, error) {
	// Set type
	t := OptionType(b[0])

	// Get Data parser function
	if fn, ok := parseOptionFns[t]; ok {
		// Pad1 is special
		if t == OptionTypePad1 {
			return fn(b[:1])
		}

		// Get the size of the data
		end := int(b[1]) + OptionHeaderLength

		// Read data into an object
		return fn(b[:end])
	}

	return nil, nil
}

func ParseOptions(b []byte) (opts Options, err error) {
	opts = make([]Option, 0)

	// Loop through data
	for len(b) > 0 {
		var o Option

		// Parse the option
		o, err = ParseOption(b)

		// Check error
		if err != nil {
			return
		}

		// Set b to after this option
		b = b[o.Len():]

		// Append options slice
		opts = append(opts, o)
	}

	return
}
