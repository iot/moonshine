package controlmessage

import "net"

type TransitInformation struct {
	External      bool
	PathControl   byte
	PathSequence  uint8
	PathLifetime  uint8
	ParentAddress net.IP
}

const (
	TransitInformationType                 = OptionTypeTransitInformation
	TransitInformationHeaderSize       int = 4
	TransitInformationPacketHeaderSize     = TransitInformationHeaderSize + OptionHeaderLength
)

func (o *TransitInformation) Len() int {
	return TransitInformationPacketHeaderSize + len(o.ParentAddress)
}

func (o *TransitInformation) Type() OptionType {
	return TransitInformationType
}

func (o *TransitInformation) Marshal() (b []byte, err error) {
	// Create byte header
	b = []byte{
		byte(TransitInformationType),
		byte(TransitInformationHeaderSize + len(o.ParentAddress)),
		BoolToByte(o.External) << 7,
		o.PathControl,
		o.PathSequence,
		o.PathLifetime,
	}

	// Append Parent Address
	b = append(b, o.ParentAddress...)

	return
}

func parseTransitInformation(b []byte) (o Option, err error) {
	// Create object
	o = &TransitInformation{
		External:      ByteToBool(b[2] >> 7),
		PathControl:   b[3],
		PathSequence:  b[4],
		PathLifetime:  b[5],
		ParentAddress: b[6:],
	}

	return
}
