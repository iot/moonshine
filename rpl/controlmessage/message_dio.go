package controlmessage

import (
	"encoding/binary"
	"net"
)

type DIO struct {
	Grounded        bool
	ModeOfOperation uint8
	DODAGPreference uint8
	VersionNumber   uint8
	Rank            uint16
	RPLInstanceID   uint8
	DTSN            uint8
	DODAGID         net.IP
}

type DIOOption byte

const (
	DIOLength int = 24
)

func (d *DIO) Len() int {
	return DIOLength
}

func (d *DIO) Type() Type {
	return TypeDIO
}

// See RFC6550 section 6.3.1.
func (d *DIO) Marshal() (b []byte, err error) {
	b = make([]byte, DIOLength)

	// Concat Grounded, ModeOfOperation and DODAGPreference
	b[4] = BoolToByte(d.Grounded)<<7 | (d.ModeOfOperation&0x07)<<3 | (d.DODAGPreference & 0x07)

	// Save byte values
	b[0] = d.RPLInstanceID
	b[1] = d.VersionNumber
	b[5] = d.DTSN

	// Convert d.Rank to a two byte slice
	binary.BigEndian.PutUint16(b[2:4], d.Rank)

	// Save DODAGID
	copy(b[8:], d.DODAGID)

	return
}

// See RFC6550 section 6.3.1.
func ParseDIO(b []byte) (Base, error) {
	var err error
	d := new(DIO)

	// Check data size
	if len(b) < DIOLength {
		return d, &SizeError{len(b), DIOLength}
	}

	// Set fields from data
	d.RPLInstanceID = b[0]
	d.VersionNumber = b[1]
	d.Rank = binary.BigEndian.Uint16(b[2:4])

	d.Grounded = ByteToBool(b[4] >> 7)
	d.ModeOfOperation = (b[4] >> 3) & 0x07
	d.DODAGPreference = b[4] & 0x07

	d.DTSN = b[5]
	d.DODAGID = net.IP(b[8:24])

	return d, err
}
