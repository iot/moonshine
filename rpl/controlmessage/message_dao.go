package controlmessage

import "net"

type DAO struct {
	RPLInstanceID uint8
	K             bool
	D             bool
	Sequence      uint8
	DODAGID       net.IP
}

type DAOOption byte

const (
	DAOLength int = 4
)

func (d *DAO) Len() int {
	return DAOLength + len(d.DODAGID)
}

func (d *DAO) Type() Type {
	return TypeDAO
}

// See RFC6550 section 6.4.1.
func (d *DAO) Marshal() (b []byte, err error) {
	b = make([]byte, DAOLength+len(d.DODAGID))

	// Set values
	b[0] = d.RPLInstanceID
	b[1] = BoolToByte(d.K)<<7 | BoolToByte(d.D)<<6
	b[3] = d.Sequence

	// Copy DODAGID
	if d.D {
		copy(b[DAOLength:], d.DODAGID)
	}

	return
}

// See RFC6550 section 6.4.1.
func ParseDAO(b []byte) (Base, error) {
	var err error
	d := new(DAO)

	// Check data size
	if len(b) < DAOLength {
		return d, &SizeError{len(b), DAOLength}
	}

	// Set fields from data
	d.RPLInstanceID = b[0]
	d.K = ByteToBool(b[1] >> 7)
	d.D = ByteToBool(b[1] >> 6)
	d.Sequence = b[3]

	// Get remaining data
	rem := b[DAOLength:]
	if d.D {
		// Store DODAGID
		d.DODAGID = rem[:16]

		// Remove DODAGID from remaining bytes
		rem = rem[16:]
	}

	return d, err
}
