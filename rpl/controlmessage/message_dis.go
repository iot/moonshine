package controlmessage

type DIS struct{}

const (
	DISLength int = 2
)

func (d *DIS) Len() int {
	return DISLength
}

func (d *DIS) Type() Type {
	return TypeDIS
}

// See RFC6550 section 6.2.1.
func (d *DIS) Marshal() (b []byte, err error) {
	b = make([]byte, DISLength)

	return
}

// See RFC6550 section 6.2.1.
func ParseDIS(b []byte) (Base, error) {
	var err error
	d := new(DIS)

	// Check data size
	if len(b) < DISLength {
		return d, &SizeError{len(b), DISLength}
	}

	return d, err
}
